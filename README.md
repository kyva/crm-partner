# Jurnal Partner MiniCRM

The Mini CRM System for Jurnal.

## How to start hacking

### Run on your native machine

- Install Ruby 2.5.1 
- Install Bundler `gem install bundler`
- Run `bundle install`
- Run `bundle exec rails s`

### Run on docker (linux-alpine, ruby 2.5.1)

1. First, Install [Docker for Mac](https://www.docker.com/docker-mac) or [Docker for Windows](https://www.docker.com/docker-windows)
2. And then run `docker-compose`

```console
$ docker-compose -f docker/docker-compose.yml up

```

Enjoy!

#### If you want to simulate for production environment

1. Edit `docker/docker-compose.yml` and uncomment the line with `RAILS_ENV=production`
2. Run compose as usual: `docker-compose -f docker/docker-compose up`

#### If you want to live-reload from your current directory (very slow on Mac)

This is used to find pesky bugs on production

1. Edit `docker/docker-compose.yml` and uncomment the line with `volumes`
2. Run compose as usual: `docker-compose -f docker/docker-compose up`
3. Edit your code and enjoy

## Environment Variables

| Name  | Required/Optional   | Purpose  |
|--:|--:|---|
| `SECRET_KEY_BASE` |  Required on production  | The secret key for production  |


## Deployment

For every merge, the app will be deployed immediately. These are the deployment information:

- URL: https://minicrm.cd.jurnal.id/
- Cloudwatch Logs: https://ap-southeast-1.console.aws.amazon.com/cloudwatch/home?region=ap-southeast-1#logStream:group=minicrm/app
- S3 Bucket for Assets: https://s3.console.aws.amazon.com/s3/buckets/jurnal-crm-assets/?region=ap-southeast-1&tab=overview

[![forthebadge](https://forthebadge.com/images/badges/built-by-hipsters.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/made-with-ruby.svg)](https://forthebadge.com)

Copyright (c) 2018 Jurnal.id
