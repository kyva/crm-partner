module Leads
  LEAD_STATUS_OPEN = "OPEN"
  LEAD_STATUS_IN_PROCESS = "IN PROCESS"

  class Lead
    attr_accessor :id, :company, :email, :phone, :status, :contact, :assignee

    def initialize id, company, email, phone, contact, assignee
      raise "Lead ID cannot be nil" if id.nil?
      raise "Invalid email during lead creation" if email.nil? || email !~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i 
      raise "Company cannot be nil" if company.nil?
      raise "Phone cannot be nil or empty" if phone.nil? || phone.empty?
      raise "Contact cannot be nil" if contact.nil?

      @id = id
      @company = company 
      @email = email
      @phone = phone
      @status = LEAD_STATUS_OPEN
      @contact = contact 
    end

    def process
      raise "Cannot process lead #{@id} when there's no assignee" if @assignee.nil?
      @status = LEAD_STATUS_IN_PROCESS 
    end
  end
end 