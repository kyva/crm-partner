module Leads
  class Company
    attr_reader :name

    def initialize name
      raise "Company name cannot be nil or empty" if name.nil? || name.empty?
    end

  end
end