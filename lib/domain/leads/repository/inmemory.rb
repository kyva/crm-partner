
module Leads 
  class LeadInMemoryRepository

    def initialize(storage)
      raise "Storage cannot be nil" if storage.nil?
      @storage = storage
    end

    def find_by_id(id)
      raise "Id cannot be nil or empty" if id.nil? || id.empty?

      return @storage.map[id]
    end

    def save(lead)
      @storage[lead.id] = lead
    end
  end
end