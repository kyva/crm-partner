module Leads

  class LeadInMemoryQueries
    def initialize(storage)
      raise "Storage cannot be nil" if storage.nil?

      @storage = storage
    end

    def find_all()
      return @storage.map.values
    end
  end
end