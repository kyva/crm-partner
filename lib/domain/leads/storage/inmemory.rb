module Leads
  class LeadsInMemoryStorage 
    attr_reader :map

    def initialize
      @map = Hash.new
    end
  end
end