Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "leads#index"
  
  resources :leads do
    member do 
      post :update_status
    end
    collection do
      post :search
    end
  end
  
  resources :activities do
    member do
      post :render_update_modal
      post :render_delete_modal
    end
  end

  get 'health', to: 'health#show'
end
