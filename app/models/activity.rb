# delete later
class Activity 
  include ActiveModel::Model

  attr_accessor :id, :status, :due_date, :description
  def initialize id, status, due_date, description
    @id = id
    @status = status
    @due_date = due_date
    @description = description
  end

  def to_key
  end
end