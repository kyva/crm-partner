# delete later
class Lead 
  include ActiveModel::Model

  attr_accessor :id, :company_name, :email, :phone, :status, :contact_name, :address, :description
  def initialize id, company_name, email, phone, status, contact_name
    @id = id
    @company_name = company_name
    @email = email
    @phone = phone
    @status = status
    @contact_name = contact_name
    @address = "dummy address"
    @description = "dummy description"
  end

  def to_key
  end
end