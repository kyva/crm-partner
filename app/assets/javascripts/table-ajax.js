function submitAjaxForm($form, $target){
  var dataSet = $form.serialize();
  return new Promise(function(resolve, reject){
    $.ajax({
      type: $form.attr("method"),
      url: $form.attr("action"),
      data: dataSet,
      success: function(data){
        if($target){
          $target.html(data);
        }
        resolve(data);
      },
      error: function(xhr){
        resolve(false);
      }
    });
  });
}

function resetSearchForm($form){
  $form[0].reset();
  $(".sort-header").attr("data-sort=", "desc");
}

$(document).on("turbolinks:load", function(){
  $("#form-search").on("submit", function(e){
    e.preventDefault();
    submitAjaxForm($(this), $("tbody", ".target-form-result"));
    return false;
  });

  $("#form-create-lead").on("submit", function(e){
    e.preventDefault();
    submitAjaxForm($(this), $("tbody", ".target-form-result")).then(function(value){
      $(".modal").modal("hide");
      resetSearchForm($("#form-search"));
      
      if(value == false){
        alert("failed to create lead");
      }else{
        alert("lead created");
      }
    });
    return false;
  });

  $("#form-update-lead").on("submit", function(e){
    e.preventDefault();
    submitAjaxForm($(this), $("tbody", ".target-form-result")).then(function(value){
      $(".modal").modal("hide");
      alert("lead updated");
    });
  });

  $("#form-create-activity").on("submit", function(e){
    e.preventDefault();
    submitAjaxForm($(this), $("tbody", ".target-form-result"));
  });


  $(".form-render-update-modal-activity").on("submit", function(e){
    e.preventDefault();
    $("#form-update-activity").detach();
    submitAjaxForm($(this), $(".target-form-render-update-modal-activity")).then(function(value){
      $("#popup-update-activity").modal("show");
    });
  });

  $("body").on("submit", "#form-update-activity", function(e){
    e.preventDefault();
    submitAjaxForm($(this)).then(function(data){
      var $row = $("tr[data-activity-id=" +  data.id +"]");
      $row.find(".activity-desc").text(data.description);
      // todo later logic status color
      $row.find(".status-color").removeClass("grey").addClass("yellow");
      $("#popup-update-activity").modal("hide");
    });
  });

  $(".form-render-delete-modal-activity").on("submit", function(e){
    e.preventDefault();
    $("#form-delete-activity").remove();
    submitAjaxForm($(this), $(".target-form-render-delete-modal-activity")).then(function(value){
      $("#popup-delete-activity").modal("show");
    });
  });

  $("body").on("submit", "#form-delete-activity", function(e){
    e.preventDefault();
    submitAjaxForm($(this)).then(function(data){
      $("#popup-delete-activity").modal("hide");
    });
  });

  $("table thead th.sort-header").on("click", function(){
    if(this.dataset.sort == "desc"){
      $(this.dataset.el).val("asc");
      this.dataset.sort = "asc";
    }else if(this.dataset.sort == "asc"){
      $(this.dataset.el).val("desc");
      this.dataset.sort = "desc";
    }

    submitAjaxForm($("#form-search"), $("tbody", ".target-form-result"));
  });

  $(".go-page").on("click", function(e){
    e.preventDefault();

    var $this = $(this);
    var $target = $("#page_no", "#form-search");
    var curr_page = parseInt($("#page-value").val());
    if($this.data('value') == 0){
      $target.val(curr_page);
    }else{
      $target.val(curr_page + $this.data('value'));
    }

    submitAjaxForm($("#form-search"), $("tbody", ".target-form-result")); 
  });
});