//= require lodash

function getData(isPaginating, baseEndpoint, currentPage, pagesAmount, $paginationElem, $scrollLoader, $window){
  isPaginating = true;
  var currentPage = parseInt($paginationElem.attr('data-current-page'));
  $paginationElem.attr('data-current-page', currentPage + 1);
  $paginationElem.show();
  $.ajax({
    url: baseEndpoint + currentPage
  }).done(function (result) {
    $('.scroll-container').append(result);
    isPaginating = false;
    if($scrollLoader.offset().top <= $window.height()){
      getData(isPaginating, baseEndpoint, currentPage, pagesAmount, $paginationElem, $scrollLoader, $window);
    }

    if (currentPage >= pagesAmount) {
      $paginationElem.hide();
      $(".scroll-loading").hide();
    }
  });
}

$(document).on("turbolinks:load", function(){
  if($("#scrolling").length > 0){
    var THRESHOLD = $(".row-header").height() + $(".filter-box").height();
    const $window = $("#scrolling");
    const $paginationElem = $('.scroll-element');
    const $scrollLoader = $(".scroll-loading");
    const $document = $('.scroll-container');
    const paginationUrl = $paginationElem.attr('data-pagination-endpoint');
    const pagesAmount = parseInt($paginationElem.attr('data-pagination-pages'));
    let currentPage = 1;
    let baseEndpoint;

    /* validate if the pagination URL has query params */
    if (paginationUrl.indexOf('?') != -1) {
      baseEndpoint = paginationUrl + "&page=";
    } else {
      baseEndpoint = paginationUrl + "?page="
    }

    /* initialize pagination */
    $paginationElem.hide();
    let isPaginating = false;
    if($scrollLoader.offset().top <= $window.height()){
      getData(isPaginating, baseEndpoint, currentPage, pagesAmount, $paginationElem, $scrollLoader, $window);
    }

    /* listen to scrolling */
    $window.on('scroll', _.debounce(function () {
      if($(window).width() > 770){
        THRESHOLD = $(".row-header").height();
      }else{
        THRESHOLD = $(".row-header").height() + $(".filter-box").height();
      }
      
      var currentPage = parseInt($paginationElem.attr('data-current-page'));
      var scroll_condition = !isPaginating && currentPage <= pagesAmount && $window.scrollTop() - THRESHOLD > $document.height() - $window.height();
      if (currentPage == 1 || scroll_condition) {
        getData(isPaginating, baseEndpoint, currentPage, pagesAmount, $paginationElem, $scrollLoader, $window);
      }
    }, 100));
  }
})