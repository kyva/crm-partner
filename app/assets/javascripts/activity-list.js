function toggleStatusDisabled(value){
  if(value){
    $("input, label", "#target-toggle").removeAttr("disabled");
  }else{
    $("input, label", "#target-toggle").attr("disabled", !value);
  }
}

$(document).on("turbolinks:load", function(){
  if(document.getElementById("input-toggle") != null){
    var status = document.getElementById("input-toggle").checked;
    toggleStatusDisabled(status);
  }

  $(document).on("change", "#input-toggle", function(){
    var status = this.checked;
    toggleStatusDisabled(status);
  });

  $(document).on("show.bs.modal", "#popup-update-activity", function(){
    var status = $("#input-toggle")[0].checked;
    toggleStatusDisabled(status);
  });
});