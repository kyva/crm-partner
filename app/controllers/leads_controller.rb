require "#{Rails.root}/lib/domain/leads/leads"

class LeadsController < ApplicationController
  before_action :init_repo_queries

  def index
    @first_page = 1
    @last_page = 10
    @industry_lists = [["Retail/Online Shop", 1], ["Jasa", 2], ["Konstruksi", 2]]
  end

  def create
    render :partial => 'lists'
  end

  def show
    @industry_lists = [["Retail/Online Shop", 1], ["Jasa", 2], ["Konstruksi", 2]]
    @lists = @query_results.take(4)
    @total_pages = 4
    @lead = Lead.new(1, SecureRandom.base64(8), ["email a", "email b"], "081222", "new lead", "Sakura")
    @query_results = [
        Lead.new(1, SecureRandom.base64(8), ["email a", "email b"], "081222", "new lead", "Budi"), 
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), ["email a", "email b"], "081222", "closed", "Budi"),
        Lead.new(4, "hello there", ["email a", "email b"], "081333", "new lead", "Budi")]
  end

  def update
  end

  def destroy
    redirect_to leads_path
  end

  def search
    @query_results = @query_results.sort_by{|x| -x.company_name.downcase}
    
    render :partial => 'lists'
  end

  def update_status
    params[:id] # lead = Lead.find(params[:id])
    params[:lead_status_id] # update lead status with this
    redirect_to lead_path(params[:id])
  end

  def init_repo_queries

    dummy_data = Leads::Lead.new(1,"Maju Mundur Kena","a@b.com","+201241484","Anu", "Itu")

    #@leads_status = [{key: 'value', key: 'value'}, [{key: 'value', key: 'value'}, [{key: 'value', key: 'value'}]]]
    # jadi kl mw dgroup navigasiny kasi array isiny:  [ {hash nav parent}, [ {hashes of nav children} ]]
    @leads_status_filter = [{id: 1, name: "All", count: 23}, 
                      {id: 2, name: "New Leads", count: 4},
                      {id: 3, name: "In Progress", count: 5},
                      [{name: "Closed"}, 
                        [{id: 5, name: "Won", count: 6},
                          {id: 6, name: "Lost", count: 1},
                          {id: 7, name: "Junk", count: 2},
                          {id: 8, name: "Unreachable", count: 0},
                          {id: 9, name: "Underqualified", count: 11},
                          {id: 10, name: "Void/Duplicate", count: 2},
                          {id: 11, name: "Product/Feature", count: 3}]]]

    @leads_status = [["new lead", "new lead"], ["closed", "closed"], ["in progress", "in progress"], ["won", "won"]]

    @query_results = [
        Lead.new(1, SecureRandom.base64(8), "email a", "081222", "new lead", "Budi"), 
        Lead.new(2, SecureRandom.base64(8), "email b", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), "email b", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), "email b", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), "email b", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), "email b", "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), "email b", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), "email b", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(2, SecureRandom.base64(8), "email b", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi"),
        Lead.new(4, "hello there", "email a", "081333", "new lead", "Budi")]
  end
end
