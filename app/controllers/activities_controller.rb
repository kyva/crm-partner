class ActivitiesController < ApplicationController
  before_action :inject_data

  def show
    @total_pages = 4
    @current_pages = 1
    render partial: 'activities/lists'
  end

  def new
  end

  def create
    params[:status] || "false" #checkbox ngirimny bentuk string
    params[:due_date] || nil
    params[:description] || nil

    if params[:status].present? #temporary, later change to if model.save
      redirect_to lead_path(params[:lead_id])
    end
  end

  def update
    render json: {id: 1, status: "true", due_date: Time.now, description: "Random Cat"}
  end

  def destroy
    redirect_to lead_path(params[:lead_id])
  end

  def render_update_modal
    @activity = Activity.new(2, true, Time.now(), "Random desc")
    render :partial => 'popup_update_activity'
  end

  def render_delete_modal
    @activity = Activity.new(2, true, Time.now(), "Random desc")
    render :partial => 'helpers/popup_delete', locals: {popup_id: "popup-delete-activity",delete_url: activity_path(params[:id], lead_id: params[:lead_id]), prompt_delete: t("activity.prompt-delete")}
  end

  def inject_data
    @lists = [
        Lead.new(1, SecureRandom.base64(8), "email a", "081222", "new lead", "Budi"), 
        Lead.new(2, SecureRandom.base64(8), "email b", "081222", "closed", "Budi"),
        Lead.new(3, SecureRandom.base64(8), "email c", "081222", "closed", "Budi")]
  end
end
